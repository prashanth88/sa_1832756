/* BSD 2-Clause License:
 * Copyright (c) 2009 - 2015
 * Software Technology Group
 * Department of Computer Science
 * Technische Universität Darmstadt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.opalj.eclipse.views

import org.eclipse.swt.SWT
import org.eclipse.swt.browser.Browser
import org.eclipse.swt.browser.LocationEvent
import org.eclipse.swt.browser.LocationListener
import org.eclipse.swt.browser.ProgressEvent
import org.eclipse.swt.browser.ProgressListener
import org.eclipse.swt.widgets.Composite
import org.eclipse.ui.part.ViewPart

import org.eclipse.jdt.core.ICompilationUnit
import org.opalj.eclipse.FindBUnit
import org.opalj.da.ClassFileReader.ClassFile
import org.opalj.da.Constant_Pool_Entry
import org.opalj.eclipse.Activator
import org.eclipse.swt.widgets.Listener
import org.eclipse.swt.widgets.Event
import org.eclipse.swt.layout.FormData
import org.eclipse.swt.layout.FormAttachment
import org.eclipse.swt.widgets.Button
import org.eclipse.swt.layout.GridLayout
import scala.collection.mutable.Stack
import java.io.FileWriter
import java.io.BufferedWriter
import java.io.File
import org.eclipse.swt.layout.FormLayout
import org.eclipse.swt.layout.RowLayout
import org.eclipse.swt.layout.RowData

object DisassemblerView {
    val Id: String = "org.opalj.eclipse.views.DisassemblerView"
    val classNameToLinkJS: String =
        """document.querySelectorAll(".fqn").forEach(function(e) {
        |  const title = e.getAttribute("title");
        |  // delete leading whitespace and periods, also delete trailing square bracket
        |  const text = e.innerHTML.replace(/^[.,\s]+/gm,'').replace(/[\[\]]+$/gm,'');
        |  const parent = e.parentNode;
        |  var fqn;
        |  if (e.getAttribute("title") != null) {
        |    fqn = e.getAttribute("title") + text;
        |  } else {
        |    fqn = text;
        |  }
        |
        |  // skip primitive types
        |  const primitives = ['void', 'byte', 'short', 'int', 'long',
        |                      'float', 'double', 'boolean', 'char'];
        |  if(primitives.indexOf(fqn) !== -1) return;
        |
        |  var link = document.createElement("a");
        |  link.setAttribute("href", fqn);
        |  link.innerHTML = e.innerHTML
        |  parent.replaceChild(link, e);
        |});""".stripMargin;
}

/**
 * Eclipse view for displaying class file disassembly
 * @author Lukas Becker
 * @author Simon Bohlender
 * @author Simon Guendling
 * @author Felix Zoeller
 */
class DisassemblerView extends ViewPart {
    var activePojectId: String = null //project id of selected unit for disassembling 
    var browser: Browser = _
    var back: Button = null //browser back navigation
    val pageStack = new Stack[String] //page stack of previously disassembled units

    def createPartControl(parent: Composite): Unit = {
        //create layout for the view
        var controls: Composite = new Composite(parent, SWT.NONE)
        
        var rowLayout: RowLayout = new RowLayout();
        rowLayout.wrap = false;
        rowLayout.pack = true;
        rowLayout.marginLeft = 5;
        rowLayout.marginTop = 5;
        rowLayout.marginRight = 5;
        rowLayout.marginBottom = 5;
        rowLayout.spacing = 0;
        
        controls.setLayout(rowLayout)
        
        back = new Button(controls, SWT.PUSH)
        back.setText("back")
        back.setEnabled(false)
        back.setLayoutData(new RowData(50, 30))
        
        back.addListener(SWT.Selection, new Listener() {
             def handleEvent(event: Event): Unit = {
                 backClicked(pageStack.pop())
             }
        })
        
        browser = new Browser(controls, SWT.NONE)
        browser.setLayoutData(new RowData(1000, 600))      
  
        browser.addProgressListener(new ProgressListener() {
            def changed(x$1: ProgressEvent): Unit = {}

            def completed(x$1: ProgressEvent): Unit = {
                browser.execute(DisassemblerView.classNameToLinkJS)
            }
        })
        browser.addLocationListener(new LocationListener() {
            def changed(l: LocationEvent): Unit = {
                classClicked(l.location.split(":")(1))       
            }
            def changing(l: LocationEvent): Unit = {
                if (l.location.startsWith("file://")) {
                    l.doit = false
                }
            }
        })
    }
    
    def backClicked(name: String): Unit = {
        if(pageStack.isEmpty) {
            back.setEnabled(false)
            pageStack.push(name)
        }
        updateBrowser(name)
    }
    
    def classClicked(name: String): Unit = {
        if(!name.equals("blank")){//ignore location listener called on page load
            back.setEnabled(true)
            if(!pageStack.top.equals(name)) {
                //location listener fires twice(once on click and again on browser settext -> ignore)
                pageStack.push(name)
            }
            updateBrowser(name)
        }
    }
    
    def updateBrowser(name: String): Unit = {
        //find class in project
        val classObj: ClassFile = FindBUnit.unitLookup(name, activePojectId)
        val file = classOf[Activator].getResourceAsStream("da.css")
        val css = scala.io.Source.fromInputStream(file).mkString
        val html: String = classObj.toXHTML(css).toString
        
        val cpe: Constant_Pool_Entry = classObj.constant_pool(classObj.this_class)
        setPartName(cpe.toString(classObj.constant_pool).split('.').last)
        setText(html)
    }
    
    def initStack(className: String) = {
        //init page stack with first class
        pageStack.push(className)
    }
    
    def setFocus(): Unit = {
        browser.setFocus
    }

    def setText(text: String): Unit = {
        browser.setText(text)
    }
    
    def setActivePojectId(activePojectId: String): Unit = {
        this.activePojectId = activePojectId
    }

    override def setPartName(text: String): Unit = {
        super.setPartName(text)
    }
}