package org.opalj.eclipse

import org.eclipse.jdt.core.search.IJavaSearchConstants
import org.eclipse.jdt.core.ICompilationUnit
import org.eclipse.core.runtime.NullProgressMonitor
import org.eclipse.jdt.core.IJavaElement
import org.eclipse.core.resources.IWorkspaceRoot
import org.eclipse.core.resources.IProject
import org.eclipse.jdt.core.IJavaProject
import org.eclipse.core.resources.ResourcesPlugin
import org.eclipse.jdt.core.JavaCore
import org.eclipse.core.runtime.IPath
import org.eclipse.jdt.core.IPackageFragment
import org.eclipse.jdt.core.IPackageFragmentRoot
import org.opalj.da.ClassFileReader.ClassFile
import org.opalj.da.ClassFileReader.AllClassFiles
import java.io.File

/**
 * Helpers to find the bytecode unit given the name
 *
 * @author Prashanth Sadanand
 */
object FindBUnit {
  /**
   * Helper to find the bytecode for a unit with a given the name inside the packages of a project
   */
  def findClassInJars(jProject: IJavaProject, name: String): ClassFile = {
      var classFile: ClassFile = null
      val jars: List[IPackageFragmentRoot] = jProject.getPackageFragmentRoots().toList
      jars.filter{j => j.getKind.equals(IPackageFragmentRoot.K_BINARY) && j.isArchive()}
      //select all archives in the java project
            
      for (jar: IPackageFragmentRoot <- jars
              if classFile == null) {
          jar.open(new NullProgressMonitor)
          val jarPath: String = jar.getPath.toOSString()
          try{
              classFile = ClassFile(jarPath, name.replace(".", "/") + ".class").head
          } catch {
            case e: Exception => println("exception caught: " + e);
          }
      }
      classFile
  }
  
  def getClassPaths(project: IJavaProject, pathInPackage: String): List[IPath] = {
      val defaultPath = project.getOutputLocation.removeFirstSegments(1)
      val projectPaths = project.getRawClasspath.toList.map { _.getOutputLocation }
      val validProjectPaths = projectPaths.filter { _ != null }
      val relativeProjectPaths = validProjectPaths.map { _.removeFirstSegments(1) }.distinct
      val workspacePath = project.getProject.getLocation
      val binaryPaths = if (relativeProjectPaths.length == 0) {
          List(workspacePath.append(defaultPath))
      } else {
          relativeProjectPaths.map { e ⇒ workspacePath.append(e) }
      }
      binaryPaths.map { path ⇒ path.append(pathInPackage) }
  }
  
  /**
   * Helper to find the bytecode for a unit with a given the name inside the output folder of a given project
   */
  def findClassInSource(jProject: IJavaProject, name: String): ClassFile = {
      var classFile: ClassFile = null
      val n = name.lastIndexOf(".")
       
      val packageName: String = if(n < 0) "" else name.substring(0, n)
      packageName.replace(".", "/")
       
      val unitName: String = name.substring(n + 1)
      
      //search for binary unit in classpaths
      val absolutePaths: List[IPath] = getClassPaths(jProject, packageName)          
      def hasClass(e: IPath): Boolean =
          e.toFile.exists && e.toFile.list.contains(unitName+".class")
      val validPaths: List[IPath] = absolutePaths.filter(hasClass)
      
      def toRelatedFiles(e: IPath): List[File] =
          e.toFile.listFiles.filter(isRelated).toList
      def isRelated(e: File): Boolean =
          e.getAbsolutePath.matches(""".*(/|\\)"""+unitName+"""(\$.*)?\.class""")
      val relatedFiles: List[File] = validPaths.map(toRelatedFiles).flatten
      
      try{
          val classFiles: List[ClassFile] = AllClassFiles(relatedFiles).map(_._1).toList
          classFiles.length match {
              case n if n > 0 ⇒
                  classFile = classFiles.head //return matched class
          }
      } catch {
        case e: Exception => println("exception caught: " + e);
      }

      classFile
  }
  
  /**
   * Helper to find the bytecode for a unit with a given the name inside the active project
   * return the ClassFile representation found first
   */
  def unitLookup(name: String, activeProject: String): ClassFile = {
       var result: ClassFile = null
       
       val workspaceRoot: IWorkspaceRoot = ResourcesPlugin.getWorkspace().getRoot()
       val projects: List[IProject] = workspaceRoot.getProjects().toList
       projects.filter{x => x.isOpen() && x.hasNature(JavaCore.NATURE_ID)}
       //select all java projects in workspace
       
       for (project: IProject <- projects
               if result == null) {
            var jProject: IJavaProject = JavaCore.create(project)
            
            if(jProject.getHandleIdentifier.equals(activeProject)) {
                //select the active project
                result = findClassInJars(jProject, name) //try to find in jars
                if(result == null) {
                    result = findClassInSource(jProject, name) //try to find in output folder
                }
            }
       }
       result
  }
}