/* BSD 2-Clause License:
 * Copyright (c) 2009 - 2014
 * Software Technology Group
 * Department of Computer Science
 * Technische Universität Darmstadt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.opalj
package da

import scala.xml.Node
import scala.collection.immutable.HashSet
import org.opalj.br._
import org.opalj.br.reader.SignatureParser

/**
 * @author Michael Eichberg
 * @author Wael Alkhatib
 * @author Isbel Isbel
 * @author Noorulla Sharief
 * @author Andre Pacak
 */
case class LocalVariableTypeTableEntry(
        start_pc:        Int,
        length:          Int,
        name_index:      Int,
        signature_index: Int,
        index:           Int
) {

    def referencedConstantPoolIndices(
        implicit
        cp: Constant_Pool
    ): HashSet[Constant_Pool_Index] = {
        HashSet(name_index, signature_index)

    }
    def toXHTML(implicit cp: Constant_Pool): Node = {
        val name = cp(name_index).toString(cp)
        val signature = cp(signature_index).asString // TODO "Decipher the signature"
        <div class="local_variable">
            <span class="pc">pc=[{ start_pc } &rarr; { start_pc + length })</span>
            /
            <span> </span><span class="local_variable_index">lv={ index }</span>
            &rArr;
            <span> </span><span class="local_variable_name">{ name }</span>
            :
            <span class="signature">
                {
                    decipher(signature)
                }
            </span>
        </div>
    }
    def decipher(sig: String): Node = {
        val fts = SignatureParser.parseFieldTypeSignature(sig)
        typeSigToXHTML(fts)
    }

    def typeSigToXHTML(ts: TypeSignature): Node = ts match {
        case _: BooleanType ⇒
            <span>boolean</span>
        case _: ByteType ⇒
            <span>byte</span>
        case _: CharType ⇒
            <span>char</span>
        case _: DoubleType ⇒
            <span>double</span>
        case _: FloatType ⇒
            <span>float</span>
        case _: IntegerType ⇒
            <span>int</span>
        case _: LongType ⇒
            <span>long</span>
        case _: ShortType ⇒
            <span>short</span>

        case scts: SimpleClassTypeSignature ⇒
            <span>
                <span class="fqn">{ scts.simpleName }</span>
                &lt;{ scts.typeArguments.map(typeArgToXHTML) }
                &gt;
            </span>
        case ats: ArrayTypeSignature ⇒
            <span>[{ typeSigToXHTML(ats.typeSignature) }</span>
        case tvs: TypeVariableSignature ⇒
            <span>T{ tvs.identifier };&nbsp;</span>
        case cts: ClassTypeSignature ⇒
            <span>
                L
                {
                    if (cts.packageIdentifier.isDefined)
                        <span class="fqn">
                            { cts.packageIdentifier.get + cts.simpleClassTypeSignature.simpleName }
                        </span>
                    else
                        <span>
                            { cts.simpleClassTypeSignature.simpleName }
                        </span>
                }
                &lt;
                { cts.simpleClassTypeSignature.typeArguments.map { typeArgToXHTML(_) } }
                &gt;
                {
                    cts.classTypeSignatureSuffix.map {
                        e ⇒ e.simpleName + e.typeArguments.map { typeArgToXHTML(_) }
                    }
                }
                ;&nbsp;
            </span>
        case _ ⇒ <span>{ ts.toJVMSignature }</span>
    }

    def typeArgToXHTML(ta: TypeArgument): Node = {
        ta match {
            case w: Wildcard ⇒
                <span>*</span>
            case prop: ProperTypeArgument ⇒
                <span>
                    { if (prop.varianceIndicator.isDefined) prop.varianceIndicator.get.toJVMSignature }
                    { typeSigToXHTML(prop.fieldTypeSignature) }
                </span>
            case _ ⇒
                // this is a complete list of TypeArugments at the time of writing
                throw new IllegalArgumentException("unknown TypeArugment")
        }
    }
}
