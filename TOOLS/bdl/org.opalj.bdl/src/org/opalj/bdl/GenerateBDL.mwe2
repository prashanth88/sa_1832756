/* BSD 2-Clause License:
 * Copyright (c) 2009 - 2015
 * Software Technology Group
 * Department of Computer Science
 * Technische Universität Darmstadt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
// Author: Thorsten Jacobi

module org.opalj.bdl.GenerateBDL

import org.eclipse.emf.mwe.utils.*
import org.eclipse.xtext.generator.*
import org.eclipse.xtext.ui.generator.*

var grammarURI = "classpath:/org/opalj/bdl/BDL.xtext"
var fileExtensions = "bdl"
var projectName = "org.opalj.bdl"
var runtimeProject = "../${projectName}"
var generateXtendStub = true
var encoding = "UTF-8"

Workflow {
    bean = StandaloneSetup {
    	scanClassPath = true
    	platformUri = "${runtimeProject}/.."
    	// The following two lines can be removed, if Xbase is not used.
    	registerGeneratedEPackage = "org.eclipse.xtext.xbase.XbasePackage"
    	registerGenModelFile = "platform:/resource/org.eclipse.xtext.xbase/model/Xbase.genmodel"
    }
    
    component = DirectoryCleaner {
    	directory = "${runtimeProject}/src-gen"
    }
    
    component = DirectoryCleaner {
    	directory = "${runtimeProject}/model/generated"
    }
    
    component = DirectoryCleaner {
    	directory = "${runtimeProject}.ui/src-gen"
    }
    
    component = DirectoryCleaner {
    	directory = "${runtimeProject}.tests/src-gen"
    }
    
    component = Generator {
    	pathRtProject = runtimeProject
    	pathUiProject = "${runtimeProject}.ui"
    	pathTestProject = "${runtimeProject}.tests"
    	projectNameRt = projectName
    	projectNameUi = "${projectName}.ui"
    	encoding = encoding
    	language = auto-inject {
    		uri = grammarURI
    
    		// Java API to access grammar elements (required by several other fragments)
    		fragment = grammarAccess.GrammarAccessFragment auto-inject {}
    
    		// generates Java API for the generated EPackages
    		fragment = ecore.EMFGeneratorFragment auto-inject {}
    
    		// the old serialization component
    		// fragment = parseTreeConstructor.ParseTreeConstructorFragment auto-inject {}    
    
    		// serializer 2.0
    		fragment = serializer.SerializerFragment auto-inject {
    			generateStub = false
    		}
    
    		// a custom ResourceFactory for use with EMF
    		fragment = resourceFactory.ResourceFactoryFragment auto-inject {}
    
    		// The antlr parser generator fragment.
    		fragment = parser.antlr.XtextAntlrGeneratorFragment auto-inject {
    		//  options = {
    		//      backtrack = true
    		//  }
    		}
    
    		// Xtend-based API for validation
    		fragment = validation.ValidatorFragment auto-inject {
    		//    composedCheck = "org.eclipse.xtext.validation.ImportUriValidator"
    		//    composedCheck = "org.eclipse.xtext.validation.NamesAreUniqueValidator"
    		}
    
    		// old scoping and exporting API
    		// fragment = scoping.ImportURIScopingFragment auto-inject {}
    		// fragment = exporting.SimpleNamesFragment auto-inject {}
    
    		// scoping and exporting API
    		fragment = scoping.ImportNamespacesScopingFragment auto-inject {}
    		fragment = exporting.QualifiedNamesFragment auto-inject {}
    		fragment = builder.BuilderIntegrationFragment auto-inject {}
    
    		// generator API
    		fragment = generator.GeneratorFragment auto-inject {}
    
    		// formatter API
    		fragment = formatting.FormatterFragment auto-inject {}
    
    		// labeling API
    		fragment = labeling.LabelProviderFragment auto-inject {}
    
    		// outline API
    		fragment = outline.OutlineTreeProviderFragment auto-inject {}
    		fragment = outline.QuickOutlineFragment auto-inject {}
    
    		// quickfix API
    		fragment = quickfix.QuickfixProviderFragment auto-inject {}
    
    		// content assist API
    		fragment = contentAssist.ContentAssistFragment auto-inject {}
    
    		// generates a more lightweight Antlr parser and lexer tailored for content assist
    		fragment = parser.antlr.XtextAntlrUiGeneratorFragment auto-inject {}
    
    		// generates junit test support classes into Generator#pathTestProject
    		fragment = junit.Junit4Fragment auto-inject {}
    
    		// rename refactoring
    		fragment = refactoring.RefactorElementNameFragment auto-inject {}
    
    		// provides the necessary bindings for java types integration
    		fragment = types.TypesGeneratorFragment auto-inject {}
    
    		// generates the required bindings only if the grammar inherits from Xbase
    		fragment = xbase.XbaseGeneratorFragment auto-inject {}
    		
    		// generates the required bindings only if the grammar inherits from Xtype
    		fragment = xbase.XtypeGeneratorFragment auto-inject {}
    
    		// provides a preference page for template proposals
    		fragment = templates.CodetemplatesGeneratorFragment auto-inject {}
    
    		// provides a compare view
    		fragment = compare.CompareFragment auto-inject {}
    	}
    }
}

